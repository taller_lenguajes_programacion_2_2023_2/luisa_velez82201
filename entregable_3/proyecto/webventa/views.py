from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from .models import Productos
# Create your views here.


def store(request):

    n_prod = Productos.objects.count()
    listado_prod = Productos.objects.all()
    contexto={"n_prod": n_prod, "lista_p":listado_prod    }
    if request.method == 'POST':
        return render(request,'store',contexto)
    else:
        return render(request,'store',contexto)


def detalle_producto(request,id):
    producto = get_object_or_404(Productos,pk=id)
    return render(request,'store.html',{'producto':producto})
