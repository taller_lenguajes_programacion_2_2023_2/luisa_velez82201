from conexion import Conexion
import pandas as pd

class Producto(Conexion):
    def __init__(self,id_producto=0.0,talla="",color="",tipo_estampado=0.0,cantidad=0.0, precio=0.0) :
        """Esta es la clase Producto

        Args:
            id_producto (float, optional): _description_. Defaults to 0.0.
            talla (str, optional): _description_. Defaults to "".
            color (str, optional): _description_. Defaults to "".
            tipo_estampado (float, optional): _description_. Defaults to 0.0.
            cantidad (float, optional): _description_. Defaults to 0.0.
            precio (float, optional): _description_. Defaults to 0.0.
        """
        self.__id_producto=id_producto     
        self.__talla=talla
        self.__color=color
        self.__tipo_estampado=tipo_estampado
        self.__cantidad=cantidad
        self.__precio=precio
        super().__init__()
        self.create_Pro()
    
        
        
    def create_Pro(self):
        #atributos = vars(self)
        if self.crear_tabla(nom_tabla="productos",datos_tbl="datos_pro"):
            print("Tabla Productos Creado!!!")
        
        return True
    
    def __str__(self) :
        return "Producto {} cantidad {} precio{}".format(self.__id_producto,self.__cantidad,self.__precio)