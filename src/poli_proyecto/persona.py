
from conexion import Conexion
import pandas as pd



class Persona (Conexion):
    def __init__(self, id_usuario = 0, nom_usuario = "", doc_usuario = 0, dire_usuario = "", tel_usuario = 0):
        """Esta es la clase Persona

        Args:
            id_usuario (int, optional): _description_. Defaults to 0.
            nom_usuario (str, optional): _description_. Defaults to "".
            doc_usuario (int, optional): _description_. Defaults to 0.
            dire_usuario (str, optional): _description_. Defaults to "".
            tel_usuario (int, optional): _description_. Defaults to 0.


        Returns:
            _type_: _description_
        """
        self.__id_usuario = id_usuario
        self.__nom_usuario = nom_usuario
        self.__doc_usuario = doc_usuario
        self.__dire_usuario= dire_usuario
        self.__tel_usuario = tel_usuario
        super().__init__()
        self.create_Per()
        ruta = "luisa_velez82201/src/poli_proyecto/static/xlsx/datos_proyecto.xlsx"
        self.df = pd.read_excel(ruta,sheet_name="Persona")
        self.insert_Per()
 
        @property
        def _id_usuario(self):
            return self.__id

        @_id_usuario.setter
        def _id_usuario(self, value):
            self.__id = value

                
    def create_Per(self):
        atributos = vars(self)
        if self.crear_tabla(nom_tabla="Persona", datos_tbl = "datos_per"):
            print("Tabla Personas Creada!!!")
        
        return atributos
    
    def update_Per(self,id_usuario=0):
        return True
    
    def delete_Per(self,id_usuario=0):
        return True
    
    def select_Per(self,id_usuario=[]):
        return Persona
        
    
        
    def __str__(self) :
        """funcion para visualizar objeto persona
        Args:
            no tiene parametros 
        """
        return "Persona : id {}  nombre {}".format(self.__id_usuario,self.__nom_usuario)
