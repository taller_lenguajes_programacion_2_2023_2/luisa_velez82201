from django.contrib import admin

# Register your models here.
from .models import Usuario,Persona

admin.site.register(Persona)
admin.site.register(Usuario)
