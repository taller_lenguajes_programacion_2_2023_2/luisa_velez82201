DROP TABLE modo_pago CASCADE CONSTRAINTS;

DROP TABLE producto CASCADE CONSTRAINTS;

DROP TABLE usuario CASCADE CONSTRAINTS;

DROP TABLE factura CASCADE CONSTRAINTS;



-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE TABLE factura (
    num_factura   INTEGER NOT NULL,
    id_usuario    INTEGER NOT NULL,
    fecha         DATE NOT NULL,
    num_pago      INTEGER NOT NULL,
    total         INTEGER NOT NULL,
    id_producto   INTEGER NOT NULL,
    id_usuario_2  INTEGER NOT NULL,
    id_producto_1 INTEGER NOT NULL,
    num_pago_1    INTEGER NOT NULL
);

ALTER TABLE factura ADD CONSTRAINT factura_pk PRIMARY KEY ( num_factura );

CREATE TABLE modo_pago (
    num_pago    INTEGER NOT NULL,
    nom_usuario VARCHAR2(100) NOT NULL,
    descripcion VARCHAR2(200) NOT NULL
);

ALTER TABLE modo_pago ADD CONSTRAINT modo_pago_pk PRIMARY KEY ( num_pago );

CREATE TABLE producto (
    id_producto    INTEGER NOT NULL,
    talla          VARCHAR2(20) NOT NULL,
    color          VARCHAR2(50) NOT NULL,
    tipo_estampado BLOB NOT NULL,
    cantidad       INTEGER NOT NULL,
    precio         INTEGER
);

ALTER TABLE producto ADD CONSTRAINT producto_pk PRIMARY KEY ( id_producto );

CREATE TABLE usuario (
    id_usuario   INTEGER NOT NULL,
    nom_usuario  VARCHAR2(100) NOT NULL,
    doc_usuario  INTEGER NOT NULL,
    dire_usuario VARCHAR2(50) NOT NULL,
    tel_usuario  INTEGER NOT NULL
);

ALTER TABLE usuario ADD CONSTRAINT usuario_pk PRIMARY KEY ( id_usuario );

ALTER TABLE factura
    ADD CONSTRAINT factura_modo_pago_fk FOREIGN KEY ( num_pago_1 )
        REFERENCES modo_pago ( num_pago );

ALTER TABLE factura
    ADD CONSTRAINT factura_producto_fk FOREIGN KEY ( id_producto_1 )
        REFERENCES producto ( id_producto );

ALTER TABLE factura
    ADD CONSTRAINT factura_usuario_fk FOREIGN KEY ( id_usuario_2 )
        REFERENCES usuario ( id_usuario );
