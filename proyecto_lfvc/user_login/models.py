from django.db import models



# Create your models here.


class Persona(models.Model):
    id_usuario = models.IntegerField(primary_key=True)
    nom_usuario = models.CharField(max_length=100)
    doc_usuario = models.CharField(max_length=100)
    dire_usuario = models.CharField(max_length=100)
    tel_usuario = models.CharField(max_length=100)