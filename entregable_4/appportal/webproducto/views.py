from django.shortcuts import render,get_object_or_404
from .models import Producto
# Create your views here.


def portal(request):
    # return HttpResponse("estoy en la pagina de inicio")
    
    n_prod = Producto.objects.count()
    listado_prod = Producto.objects.all()
    contexto={"n_prod": n_prod, "lista_p":listado_prod    }
    if request.method == 'POST':
        return render(request,'portal.html',contexto)
    else:
        return render(request,'portal.html',contexto)
    
def detalle_prod(request,id):
    prod = get_object_or_404(Producto,pk=id)
    return render(request,'detalle.html',{
        'prod_id':prod})