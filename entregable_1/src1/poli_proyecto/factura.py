
from usuario import Usuario
from producto import Producto
from conexion import Conexion
from venta import Venta

class Factura(Conexion):
    def __init__(self,num_factura=0,id_usuario=0,fecha="13/09/2023", num_pago=0, total=0, id_producto=0, id_usuario_2=0, id_producto_1=0, num_pago_1=0 ) -> None:
        """Esta es la clase Factura

        Args:
            num_factura (int, optional): _description_. Defaults to 0.
            id_usuario (int, optional): _description_. Defaults to 0.
            fecha (str, optional): _description_. Defaults to "13/09/2023".
            num_pago (int, optional): _description_. Defaults to 0.
            total (int, optional): _description_. Defaults to 0.
            id_producto (int, optional): _description_. Defaults to 0.
            id_usuario_2 (int, optional): _description_. Defaults to 0.
            id_producto_1 (int, optional): _description_. Defaults to 0.
            num_pago_1 (int, optional): _description_. Defaults to 0.
        """
        self.__num_factura=num_factura
        self.__id_usuario=id_usuario
        self.__fecha=fecha
        self.__num_pago=num_pago
        self.__total=total
        self.__id_producto=id_producto
        self.__id_usuario_2=id_usuario_2
        self.__id_producto_1=id_producto_1
        self.__num_pago=num_pago
        self.__per = Usuario()
        self.__prod = Producto()
        self.__Vta = Venta()
        super().__init__()
        self.create_Vta()
        
            
    def create_Fra(self):
        if self.crear_tabla(nom_tabla="factura",datos_tbl="datos_fra"):
            print("Tabla Factura Creada!!!")
        
        return True
          
        
    def __str__(self) -> str:
        return "Factura \n num_pago {} num_factura {} \n   {} \n   {} \n id_usuario  {} total {}".format(self.__num_pago,self.__num_factura,self.__per.__str__(), self.__prod.__str__(),self.__total)