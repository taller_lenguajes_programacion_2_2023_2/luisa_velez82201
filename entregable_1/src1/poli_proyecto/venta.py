
from usuario import Usuario
from producto import Producto
from conexion import Conexion

class Venta(Conexion):

    def __init__(self,num_pago=0,nom_usuario="",descripcion="") -> None:
        """Esta es la clase Venta

        Args:
            num_pago (int, optional): _description_. Defaults to 0.
            nom_usuario (str, optional): _description_. Defaults to "".
            descripcion (str, optional): _description_. Defaults to "".
        """
        self.__num_pago=num_pago
        self.__nom_usuario=nom_usuario
        self.__per = Usuario()
        self.__prod = Producto()
        self.__descripcion=descripcion
  

        super().__init__()
        self.create_Vta()
        

            
    def create_Vta(self):
        if self.crear_tabla(nom_tabla="ventas",datos_tbl="datos_vta"):
            print("Tabla Venta Creada!!!")
        
        return True
          
        
    def __str__(self) -> str:
        return "Venta \n num_pago {} nom_usuario {} \n descripcion {} total {}".format(self.__num_pago,self.__nom_usuario,self.__per.__str__(), self.__prod.__str__(),self.__descripcion)
