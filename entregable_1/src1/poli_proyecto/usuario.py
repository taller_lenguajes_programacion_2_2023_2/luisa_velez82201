#POO: Estandar de trabajo, o paradigma de programacion
#Atributos(Caracteristicas) y Funciones (acciones)
from conexion import Conexion

class Usuario :
    def __init__(self, id_usuario = 0, nom_usuario = "", doc_usuario = 0, dire_usuario = "", tel_usuario = 0
                 ): 
        """Esta es la clase usuario

        Args:
            id_usuario (int, optional): Identificador de cada usuario. Defaults to 0.
            nom_usuario (str, optional): Nombre del usuario. Defaults to "".
            doc_usuario (int, optional): Documento del usuario. Defaults to 0.
            dire_usuario (str, optional): Direccion del usuario. Defaults to "".
            tel_usuario (int, optional): Telefono del usuario. Defaults to 0.
        """
        self.id_usuario = id_usuario
        self.nom_usuario = nom_usuario
        self.doc_usuario = doc_usuario
        self.dire_usuario = dire_usuario
        self.tel_usuario = tel_usuario
  
 
def __str__(self) :
     
     """funcion para visualizar objeto usuario
     Args:
        No tiene parametros
     """
     return  "{} objeto " .format(self.id_usuario, self.nom_usuario)
 
per = Usuario(nom_usuario="Luisa")      
per.__str__()
        
        
    