from django.db import models

# Create your models here.

class Persona(models.Model):
    nom_completo = models.IntegerField(primary_key=True)
    nom_usuario = models.CharField(max_length=100)
    email= models.CharField(max_length=100)
    contra = models.CharField(max_length=100)
    con_contra = models.CharField(max_length=100)


class Usuario(models.Model):
    nom_completo = models.IntegerField(primary_key=True)
    nom_usuario = models.CharField(max_length=100)
    email= models.CharField(max_length=100)
    contra = models.CharField(max_length=100)
    fech_registro = models.CharField(max_length=100)
    token = models.CharField(max_length=100)
